## Usage

- Use `git clone https://TonyHan9966@bitbucket.org/tonyhan9966/assignment_1.git` to clone the project in command line.

- Or use Sourcetree/VSCode to clone the project.

## License

assignment_1 is available under the MIT license. See the LICENSE file for more info.

I choice MIT license because it allows us to share our code under a copyleft license without forcing others to expose their proprietary code, it’s business friendly and open source friendly while still allowing for monetization.